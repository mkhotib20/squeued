import { apolloCLient } from '@/utils/apollo/client';
import loginMutation from '@/queries/login.gql';
import { NextApiRequest, NextApiResponse } from 'next';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body } = req;
  const { data } = await apolloCLient.query({ query: loginMutation });
  console.log(data);
  
};
