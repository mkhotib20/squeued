import 'bootstrap/dist/css/bootstrap.min.css';

import React from 'react';
import Head from 'next/head';
import { globalStyles } from '@/styles/global';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import DayJsAdapter from '@date-io/dayjs';
import { ApolloProvider } from '@apollo/client';
import { apolloCLient } from '@/utils/apollo/client';

const MyApp = ({ Component, pageProps }) => (
  <>
    <Head>
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@100;300;400;500;700;900&display=swap"
      />
      <link rel="image/svg" href="/topIcon.svg" />
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

      <link
        href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@100;300;400;500;700;900&display=swap"
        rel="stylesheet"
      />
      <title>Squeued</title>
    </Head>
    {globalStyles}
    <MuiPickersUtilsProvider utils={DayJsAdapter}>
      <ApolloProvider client={apolloCLient}>
        <Component {...pageProps} />
      </ApolloProvider>
    </MuiPickersUtilsProvider>
  </>
);

export default MyApp;
