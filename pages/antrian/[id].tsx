import Loading from '@/components/Loading';
import { withAuth } from '@/hoc/withAuth';
import dynamic from 'next/dynamic';

const HomePage = dynamic(() => import('@/pages/Antrian'), { ssr: true, loading: Loading });

export default withAuth(HomePage);
