import { AuthProvider } from '@/context/auth';
import { CookieProvider } from '@/context/cookie';
import { verify } from 'jsonwebtoken';
import { AUTH_COOKIE_KEY } from '@/utils/constant';
import { withCookie, WithCookieContext } from 'next-cookie';
import { NEXT_PUBLIC_JWT_KEY } from '@/constants';
import { redirect } from '@/helpers/general';

export const withAuth = (Component: any, access?: 'Merchant' | 'Customer' | 'Guest') => {
  const WrapedComponent: any = ({ initialAuth, cookie }) => {
    return (
      <CookieProvider cookie={cookie.cookie}>
        <AuthProvider {...initialAuth}>
          <Component />
        </AuthProvider>
      </CookieProvider>
    );
  };

  WrapedComponent.getInitialProps = async (ctx: WithCookieContext) => {
    const cookie: any = ctx.cookie.get(AUTH_COOKIE_KEY);

    const props = {
      ...(Component.getInitialProps && (await Component.getInitialProps(ctx))),
    };
    if (cookie) {
      const jwtPayload: any = verify(cookie?.token, NEXT_PUBLIC_JWT_KEY);
      if (access && jwtPayload) {
        if (access == 'Guest' && ctx.pathname == '/admin/login') {
          redirect('/admin', ctx);
          return props;
        }

        const { userType } = jwtPayload;
        if (userType == 'Guest User') {
          redirect('/admin/login', ctx);
          return props;
        }
      }
    }
    if (cookie && Boolean(Object.keys(cookie).length)) {
      props.initialAuth = cookie;
    }
    return props;
  };
  return withCookie(WrapedComponent);
};
