export const serializeFormData = (event: any): any => {
  const fd = new FormData(event.currentTarget);
  const objData = {};
  for (let [key, value] of fd.entries() as any) {
    objData[key] = value;
  }

  return objData;
};
