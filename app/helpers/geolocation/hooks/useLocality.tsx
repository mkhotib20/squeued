import { useAuthData } from '@/context/auth';
import { AUTH_COOKIE_KEY, XAppKeyContext } from '@/utils/constant';
import { useLazyQuery } from '@apollo/client';
import { useCallback, useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { verify } from 'jsonwebtoken';
import { getCurrentPosition } from '../geocode';
import authenticateQuery from '../queries/authenticateQuery.gql';
import { NEXT_PUBLIC_JWT_KEY } from '@/constants';

const useLocality = (): {
  locality: string;
  loading: boolean;
} => {
  const { setAuth } = useAuthData();
  const [locality, setLocality] = useState<string>('Locality is not supported');
  const [cookies, _] = useCookies();
  const [loading, setLoading] = useState(true);
  const [call, { error, loading: authLoading }] = useLazyQuery(authenticateQuery, {
    context: XAppKeyContext,
    onCompleted: ({ authenticate = {} }) => {
      const { locality, token } = authenticate;
      setLocality(locality?.formated);
      setLoading(false);
      setAuth(authenticate);
    },
    onError: (error) => {
      setLoading(false);
    },
  });

  const getAddress = useCallback(async () => {
    const { latitude, longitude } = (await getCurrentPosition()) || {};

    call({
      variables: {
        lat: latitude,
        lng: longitude,
      },
    });
  }, []);

  useEffect(() => {
    if (!cookies[AUTH_COOKIE_KEY]) {
      getAddress();
    } else {
      const { token, locality } = cookies[AUTH_COOKIE_KEY];
      try {
        if (verify(token, NEXT_PUBLIC_JWT_KEY)) {
          setLocality(locality.formated);
          setLoading(false);
        } else {
          getAddress();
        }
      } catch (error) {
        getAddress();
      }
    }
  }, [getAddress, cookies]);

  return { locality, loading: loading || authLoading };
};

export default useLocality;
