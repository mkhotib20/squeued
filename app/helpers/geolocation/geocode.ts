import { canUseDOM } from '@/utils/constant';
import axios from 'axios';
import { GeocodeResponse } from './interface';

const defaultLongLat: Partial<GeolocationCoordinates> = {
  latitude: -6.2,
  longitude: 106.816666,
};
export const getCurrentPosition = async (): Promise<Partial<GeolocationCoordinates>> => {
  return new Promise((ok) => {
    window.navigator.geolocation.getCurrentPosition(
      (pos) => ok(pos.coords),
      () => ok(defaultLongLat),
      { enableHighAccuracy: true }
    );
  });
};

export const getMyAddress = async (): Promise<string | undefined> => {
  if (canUseDOM()) {
    const { latitude, longitude } = (await getCurrentPosition()) || {};
    try {
      const { data } = await axios.get<GeocodeResponse>(
        `https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=${latitude}&longitude=${longitude}&localityLanguage=id`
      );
      const { locality, principalSubdivision } = data;
      return `${locality}, ${principalSubdivision}`;
    } catch (error) {
      return undefined;
    }
  }
  return undefined;
};
