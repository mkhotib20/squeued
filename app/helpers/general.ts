import _router from 'next/router';

export const redirect = (to: string, ctx?: any) => {
  if (typeof window != 'undefined') {
    _router.push(to);
  } else if (ctx && ctx.res) {
    ctx.res?.writeHead(302, { Location: to });
    ctx.res?.end('not found');
  }
};
