import { useEffect, useRef, useState } from "react";

export const useIntersect = (onIntersect: () => void, once = false) => {
  const [element, setElement] = useState<Element | null>(null);
  const observer = useRef<null | IntersectionObserver>(null);
  const ref = useRef(null);

  const cleanOb = () => {
    if (observer.current) {
      observer.current.disconnect();
    }
  };

  useEffect(() => {
    setElement(ref.current);
  }, [ref]);

  useEffect(() => {
    if (!element) return;
    cleanOb();
    const ob = (observer.current = new IntersectionObserver(([entry]) => {
      const isElementIntersecting = entry.isIntersecting;
      if (isElementIntersecting) {
        onIntersect();
      }
      // if (isElementIntersecting) {
      //   cleanOb();
      // }
    }));
    ob.observe(element);
    return () => {
      cleanOb();
    };
  }, [element]);

  return ref;
};
