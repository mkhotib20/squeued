import { useAuthData } from '@/context/auth';
import { XAppKeyContext } from '@/utils/constant';
import {
  DocumentNode,
  MutationHookOptions,
  MutationTuple,
  OperationVariables, TypedDocumentNode,
  useMutation as AMutation
} from '@apollo/client';

export function useMutation<TData = any, TVariables = OperationVariables>(
  mutation: DocumentNode | TypedDocumentNode<TData, TVariables>,
  options?: MutationHookOptions<TData, TVariables>
): MutationTuple<TData, TVariables> {
  const { token } = useAuthData();
  return AMutation(mutation, {
    ...options,
    context: { headers: { ...XAppKeyContext.headers, authorization: `Bearer ${token}` } },
  });
}

export default useMutation;
