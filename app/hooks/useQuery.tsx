import React from 'react';
import {
  DocumentNode,
  OperationVariables,
  QueryHookOptions,
  QueryResult,
  TypedDocumentNode,
  useQuery as AQuery,
} from '@apollo/client';
import { useAuthData } from '@/context/auth';
import { XAppKeyContext } from '@/utils/constant';

function useQuery<TData = any, TVariables = OperationVariables>(
  query: DocumentNode | TypedDocumentNode<TData, TVariables>,
  options?: QueryHookOptions<TData, TVariables>
): QueryResult<TData, TVariables> {
  const { token } = useAuthData();

  return AQuery(query, {
    ...options,
    context: { headers: { ...XAppKeyContext.headers, authorization: `Bearer ${token}` } },
  });
}

export default useQuery;
