import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { useCallback, useEffect, useState } from 'react';

interface AxiosHooksReponse extends AxiosResponse {
  loading: boolean;
  error: any;
}

type AxiosHooks = (options?: AxiosRequestConfig & { lazy?: boolean }) => [
  (option?: AxiosRequestConfig) => Promise<
    | AxiosResponse
    | {
        error: any;
      }
  >,
  AxiosHooksReponse
];

const useAxios: AxiosHooks = (defaultOption = {}) => {
  const { lazy, ...restOption } = defaultOption;
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<any>();
  const [response, setResponse] = useState<AxiosResponse>();

  const callAxios = useCallback(
    async (option?: AxiosRequestConfig) => {
      setLoading(true);
      try {
        const response = await axios({ ...restOption, ...option });
        setResponse(response);
        return { ...response };
      } catch (error) {
        setError(error.response?.data || error);
        return { error: error.response?.data || error };
      } finally {
        setLoading(false);
      }
    },
    [restOption]
  );

  useEffect(() => {
    if (!lazy) {
      callAxios();
    }
  }, [callAxios, lazy]);

  return [callAxios, { ...response, loading, error }];
};

export default useAxios;
