import { AUTH_COOKIE_KEY } from '@/utils/constant';
import { useCookie } from '@/context/cookie';
import React from 'react';
import { useCookies } from 'react-cookie';
import { useState } from 'react';

interface AuthContext {
  isLoggedIn: boolean;
  token?: string;
  locality: { cityId?: string; formated?: string };
  setAuth?: (value: AuthData) => void;
}

type AuthData = Omit<AuthContext, 'setAuth'>;

const defaultContext: AuthContext = {
  isLoggedIn: false,
  token: '',
  locality: {},
};

export const AuthContext = React.createContext(defaultContext);
export const useAuthData = () => React.useContext(AuthContext);
export const AuthProvider: React.FC<Partial<AuthContext>> = ({ children, ...initialValue }) => {
  const [data, setData] = useState<AuthData>();
  const [_, setCookie] = useCookies();
  const setAuth = (newData: AuthData) => {
    setData(newData);
    setCookie(AUTH_COOKIE_KEY, JSON.stringify(newData), { path: '/' });
  };
  return (
    <AuthContext.Provider
      value={{
        ...defaultContext,
        ...initialValue,
        ...data,
        setAuth,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
