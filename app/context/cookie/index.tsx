import { useCookie as useNc, Cookie } from 'next-cookie';
import React from 'react';
import { FC } from 'react';
import { useContext } from 'react';

const CookieContext = React.createContext<Partial<Cookie>>({});

export const useCookie = () => useContext(CookieContext);

export const CookieProvider: FC<any> = ({ children, cookie }) => {
  const cookieInstance = useNc(cookie);
  
  
  return <CookieContext.Provider value={cookieInstance}>{children}</CookieContext.Provider>;
};
