import { PRIMARY_COLOR } from '@/utils/constant/style';
import { Global, css } from '@emotion/react';

export const globalStyles = (
  <Global
    styles={css(`
      * {
        font-family: 'Noto Sans JP', -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
      }

      body {
        min-height: 100vh;
      }
      small {
        font-size: 11px;
      }
      label,input {
        font-size: 14px;
      }

      body {
        width: 100%;
        overflow-x: hidden;
        min-height: 100vh;
        margin: 0;
      }

      * {
        font-family: 'DM Sans', sans-serif;
      }
      .form-group{
        margin-bottom: 20px;
        label{
          margin-bottom: 8px;
        }
      }

      a {
        text-decoration: unset;
        color: unset!important;
        &:hover: {
          color: unset;
        }
      }
      .close {
        background-color: transparent;
        border-width: 0;
      }
      .form-control {
        box-shadow: unset!important;
        &:focus{
          border-color: ${PRIMARY_COLOR};
        }
      }

      .slick-dot {
        padding: 0;
        list-style: none;
        display: flex;
        text-align: center;
        margin-top: 0px;
      }

      .slick-dot>li {
        display: inline-block;
        padding: 2px;
      }

      .slick-dot>li>button {
        content: '';
        color: transparent;
        border: none;
        border-radius: 8px;
        display: block;
        width: 8px;
        font-size: 0px !important;
        height: 6px;
        background-color: #d6d6d6;
        padding: 0 !important;
        transition: all .3s ease-in;
      }

      .slick-active>button {
        background-color: #0ACF83 !important;
        width: 20px !important;
      }

      .spinner {
        animation: spin infinite 1s linear;
      }

      @keyframes spin {
        from {
          transform: rotate(0deg);
        }

        to {
          transform: rotate(360deg);
        }
      }
`)}
  />
);
