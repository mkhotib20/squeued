import { Theme } from '@emotion/react'
import { Interpolation } from '@emotion/serialize'
import React, { FC, useEffect, useState } from 'react'
import Skeleton from 'react-loading-skeleton'

const Image:FC<React.ClassAttributes<HTMLImageElement> & React.ImgHTMLAttributes<HTMLImageElement> & {
    css?: Interpolation<Theme>;
}> = (props) => {
    const {src, height, width, css} = props
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const img = document.createElement("img")
        img.src = src || ''
        img.onload = () => {
            setLoading(false)
        }
    }, [src])
    
    if (loading) {
        return <Skeleton css={css} height={height} width={width}/>
    }
    
    return (
        <img {...props} alt='squeued-assets' />
    )
}

export default Image
