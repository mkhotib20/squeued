import { Interpolation, Theme } from '@emotion/react';
import React from 'react';

export type ButtonTypes = 'primary' | 'danger' | 'muted';

export interface ButtonProps
  extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  href?: string;
  target?: string;
  as?: string;
  css?: Interpolation<Theme>;
  buttonType?: ButtonTypes;
  block?: boolean;
  loading?: boolean;
}
