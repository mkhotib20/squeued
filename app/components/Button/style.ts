import { PRIMARY_COLOR } from '@/utils/constant/style';
import { css } from '@emotion/react';
import { ButtonTypes } from './interface';

export const cssButton = (buttonType: ButtonTypes, block: boolean) => {
  let backgroundColor = '';
  let color = '';
  switch (buttonType) {
    case 'danger':
      backgroundColor = 'red';
      color = '#FFF';
      break;
    case 'muted':
      backgroundColor = 'grey';
      color = '#FFF';
      break;

    default:
      backgroundColor = PRIMARY_COLOR;
      color = '#FFF';
      break;
  }
  return css({
    backgroundColor,
    color,
    borderWidth: 0,
    borderRadius: 6,
    padding: 8,
    minWidth: 100,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    textDecoration: 'none',
    fontSize: 12,
    ...(block && {
      width: '100%',
    }),
  });
};
