import React from 'react';
import Link from 'next/link';
import { ButtonProps } from './interface';
import { cssButton } from './style';
import { CircularProgress } from '@material-ui/core';

const Button: React.FC<ButtonProps> = (props) => {
  const { href, as, target, buttonType, block, loading, ...buttonProps } = props;

  const renderAnchor = () => (
    <a {...(props as any)} css={[props.css, cssButton(buttonType, block)]}>
      {loading && <CircularProgress color="inherit" size={16} css={{ marginRight: 8 }} />} {props.children}
    </a>
  );
  if (href) {
    if (target) {
      renderAnchor();
    }

    return (
      <Link href={href} as={as}>
        {renderAnchor()}
      </Link>
    );
  }

  return (
    <button {...buttonProps} disabled={loading} css={[props.css, cssButton(buttonType, block)]}>
      {loading && <CircularProgress color="inherit" size={16} css={{ marginRight: 8 }} />} {props.children}
    </button>
  );
};

export default Button;
