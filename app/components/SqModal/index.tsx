import dynamic from 'next/dynamic';

const SqModal = dynamic(() => import('./view'), { ssr: true });

export default SqModal;
