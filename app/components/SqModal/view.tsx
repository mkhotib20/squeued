import React, { FC, useState } from 'react';
import { FaTimes } from 'react-icons/fa';
import { Modal, ModalBody, ModalHeader, ModalProps } from 'reactstrap';

const ComponentModal: FC<ModalProps> = (props) => {
  const { children, ...restProps } = props;
  return (
    <Modal {...restProps}>
      <ModalHeader toggle={(e: any) => props.toggle(e)}>{props.title}</ModalHeader>
      <ModalBody>{props.children}</ModalBody>
    </Modal>
  );
};

export default ComponentModal;
