import { HEADER_HEIGHT } from '@/utils/constant/style';
import { css } from '@emotion/react';

export const cssContentWrapper = css({
  maxWidth: 500,
  paddingTop: HEADER_HEIGHT,
  marginLeft: 'auto',
  marginRight: 'auto',
});
