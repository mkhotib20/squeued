import React, { FC } from 'react';
import BottomNavbar from './components/BottomNavbar';
import Header from './components/Header';
import { cssContentWrapper } from './style';

interface LayoutProps {
  actionBack?: boolean;
  title?: string;
  withBottomAction?: boolean;
}

const AppLayout: FC<LayoutProps> = ({ withBottomAction, children, actionBack, title }) => {
  return (
    <div>
      <Header actionBack={actionBack} title={title} />
      <div css={cssContentWrapper}>{children}</div>
      {/* Coming soon at future */}
      {/* {withBottomAction && <BottomNavbar />} */}
    </div>
  );
};

export default AppLayout;
