import React from "react";
import { FiSearch } from "react-icons/fi";
import { cssSearchHead } from "./style";

const SearchHead = () => {
  return (
    <div style={{ padding: "0 16px", width: "100%" }}>
      <div css={cssSearchHead}>
        <input type="search" autoComplete="off" autoFocus placeholder="Cari tempat antrian" />
        <span className="icon">
          <FiSearch size={22} color="#BDBDBD" />
        </span>
      </div>
    </div>
  );
};

export default SearchHead;
