import { css } from '@emotion/react';

export const cssHeaderWrapper = css({
  position: 'fixed',
  left: 0,
  right: 0,
  top: 0,
  boxShadow: '0px 3px 4px rgba(226, 226, 226, 0.25)',
  height: 56,
  zIndex: 9,
  backgroundColor: '#fff',
  maxWidth: 500,
  margin: 'auto',
  display: 'flex',
  alignItems: 'center',
  '&>.main-title': {
    margin: 'auto',
    fontSize: 30,
    fontWeight: 300,
    userSelect: 'none',
    textDecoration: 'unset',
    color: '#333',
  },
});

export const cssSearchHead = css({
  display: 'flex',
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  '&>input': {
    padding: '12px 17px 12px 47px',
    width: '100%',
    border: 'none',
    backgroundColor: '#EFEFEF',
    borderRadius: 5,
    height: '100%',
    '&:focus': {
      outline: 'none',
    },
  },
  '&>.icon': {
    position: 'absolute',
    left: 30,
    top: 15,
  },
});

export const cssHeaderWithActionBack = css({
  display: 'flex',
  alignItems: 'center',
  height: '100%',
});

export const cssBackButton = css({
  backgroundColor: 'transparent',
  border: 'none',
  height: '100%',
  display: 'block',
  width: 56
});
