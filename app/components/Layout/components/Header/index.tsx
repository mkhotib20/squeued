import { useRouter } from 'next/router';
import { FiArrowLeft } from 'react-icons/fi';
import Link from 'next/link';
import React from 'react';
import SearchHead from './SearchHead';
import { cssBackButton, cssHeaderWithActionBack, cssHeaderWrapper } from './style';

const Header = ({ actionBack = false, title = '' }) => {
  const { pathname, back } = useRouter();

  const renderHeader = () => {
    if (actionBack) {
      return (
        <div css={cssHeaderWithActionBack}>
          <button onClick={back} role="button" css={cssBackButton}>
            <FiArrowLeft color={'#7b7b7b'} size={27} />
          </button>
          <span css={{ color: '#7b7b7b', fontWeight: 600 }}>{title}</span>
        </div>
      );
    }
    return pathname !== '/search' ? (
      <Link href="/">
        <a className="main-title">
          <img src="/topIcon.svg" alt="" />
        </a>
      </Link>
    ) : (
      <SearchHead />
    );
  };
  return <div css={cssHeaderWrapper}>{renderHeader()}</div>;
};

export default Header;
