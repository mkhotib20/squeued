import React from 'react';
import { SqueuedHome, SqueuedMessage, SqueuedProfile, SqueuedScan, SqueuedSearch } from '@/icons/bottomNav';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { cssBottomFabNav, cssBottomNavbarWrapper, cssSingleNav } from './style';

const BottomNavbar = () => {
  const { pathname } = useRouter();

  return (
    <div css={cssBottomNavbarWrapper}>
      {/* <Link href="/">
        <a css={cssSingleNav(pathname === '/')}>
          <SqueuedHome active={pathname === '/'} />
        </a>
      </Link>
      <Link href="/search">
        <a css={cssSingleNav(pathname === '/search')}>
          <SqueuedSearch active={pathname === '/search'} />
        </a>
      </Link> */}
      <Link href="/scan">
        <a css={cssBottomFabNav()}>
          <SqueuedScan color="white" size={30} />
        </a>
      </Link>
      {/* <Link href="/notification">
        <a css={cssSingleNav(pathname === '/notification')}>
          <SqueuedMessage active={pathname === '/notification'} />
        </a>
      </Link>

      <Link href="/user">
        <a css={cssSingleNav(pathname === '/user')}>
          <SqueuedProfile active={pathname === '/user'} />
        </a>
      </Link> */}
    </div>
  );
};

export default BottomNavbar;
