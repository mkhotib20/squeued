import { css } from '@emotion/react';
import { PRIMARY_COLOR } from '@/utils/constant/style';

export const cssBottomNavbarWrapper = css({
  position: 'fixed',
  left: 0,
  right: 0,
  bottom: 0,
  borderTop: '1px solid #ebebeb',
  height: 56,
  zIndex: 9,
  backgroundColor: '#fff',
  maxWidth: 500,
  margin: 'auto',
  display: 'flex',
  alignItems: 'center',
});

export const cssSingleNav = (active = false) =>
  css({
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    fontSize: 20,
    color: active ? PRIMARY_COLOR : '#ababab',
  });

export const cssBottomFabNav = (active = false) =>
  css({
    width: 60,
    height: 60,
    marginLeft:'auto',
    marginRight:'auto',
    borderRadius: '50%',
    backgroundColor: PRIMARY_COLOR,
    marginBottom: 40,
    boxShadow: '0px 0px 8px #aaa',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0,
  });
