import React from 'react';
import { FC } from 'react';

interface EmptyViewProps {
  title?: string;
  height?: number;
}

const EmptyView: FC<EmptyViewProps> = ({ title, height }) => {
  return (
    <div css={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', height }}>
      <p css={{ opacity: 0.5 }}>{title || 'No Data'}</p>
    </div>
  );
};

export default EmptyView;
