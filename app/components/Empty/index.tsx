import dynamic from 'next/dynamic';
import Loading from '@/components/Loading';

const EmptyState = dynamic(() => import('./view'), { ssr: true, loading: Loading });

export default EmptyState;
