import { css } from '@emotion/react';

export const cssLoaderStyle = css({
  display: 'flex',
  position: 'fixed',
  zIndex: 9999,
  inset: 0,
  height: '100vh',
  textAlign: 'center',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'rgba(255,255,255,.5)',
});
