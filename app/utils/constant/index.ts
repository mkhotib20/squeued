export const canUseDOM = () => typeof window !== 'undefined';
export const AUTH_COOKIE_KEY = 'session-squeued';
export const AUTH_COOKIE_LONGLAT_KEY = 'session-longlat';

export const XAppKeyContext = {
  headers: {
    'x-app-key': '7c489ec8-1286-47cb-9ff4-14c0676d53fc',
  },
};
