import { createWithApollo } from './createWithApollo';
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { NextPageContext } from 'next';
import { NEXT_PUBLIC_BACKEND_URL } from '@/constants';

const createClient = (ctx: NextPageContext) =>
  new ApolloClient({
    cache: new InMemoryCache(),
    link: createHttpLink({
      uri: NEXT_PUBLIC_BACKEND_URL,
    }),
  });

export const withApollo = createWithApollo(createClient);
