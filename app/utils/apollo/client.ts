import { NEXT_PUBLIC_BACKEND_URL } from '@/constants';
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';

export const apolloCLient = new ApolloClient({
  cache: new InMemoryCache(),
  link: createHttpLink({
    uri: NEXT_PUBLIC_BACKEND_URL,
  }),
});
