import { withAuth } from '@/hoc/withAuth';
import dynamic from 'next/dynamic';

const AdminHomepage = dynamic(() => import('./view'));

export default withAuth(AdminHomepage, 'Merchant');
