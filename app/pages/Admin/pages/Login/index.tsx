import { withAuth } from '@/hoc/withAuth';
import dynamic from 'next/dynamic';

const AdminLoginPage = dynamic(() => import('./view'));

export default withAuth(AdminLoginPage, 'Guest');
