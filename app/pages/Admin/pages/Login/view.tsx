import React from 'react';

import Button from '@/components/Button';
import { useAuthData } from '@/context/auth';
import useMutation from '@/hooks/useMutation';
import AppLayout from '@/components/Layout';
import { serializeFormData } from '@/helpers/form';
import { FormControl, Input, InputLabel } from '@material-ui/core';
import loginMutation from './queries/login.gql';

const ComponentAdminLogin = () => {
  const { setAuth } = useAuthData();
  const [doLogin, { data, loading }] = useMutation(loginMutation, {
    onCompleted: ({ login: { token, locality } }) => {
      setAuth({ isLoggedIn: true, token, locality });
    },
    onError: (error) => {
      console.log(error);
    },
  });

  const handleLogin: React.FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    const data = serializeFormData(e);
    doLogin({ variables: data });
  };

  return (
    <AppLayout>
      <div css={{ padding: 16 }}>
        <div css={{ textAlign: 'center', marginTop: 70 }}>
          <img css={{ width: 100 }} src="/icon.svg" alt="squeued-icon" />
        </div>
        <h5 css={{ textAlign: 'center', marginTop: 30, marginBottom: 30 }}>Untuk melanjutkan harap login</h5>
        <div>
          <form onSubmit={handleLogin} css={{ width: '100%' }}>
            <FormControl fullWidth>
              <InputLabel htmlFor="email">Email address</InputLabel>
              <Input name="email" />
            </FormControl>
            <FormControl style={{ marginTop: 20 }} fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input type="password" name="password" />
            </FormControl>
            <Button loading={loading} block css={{ marginTop: 30, padding: '12px' }}>
              LOGIN
            </Button>
          </form>
        </div>
      </div>
    </AppLayout>
  );
};

export default ComponentAdminLogin;
