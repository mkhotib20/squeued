import { PRIMARY_COLOR } from '@/utils/constant/style';
import { css } from '@emotion/react';

export const cssMerchantTitle = css({
  marginBottom: 10,
  flex: 1,
  marginTop: 0,
  fontWeight: 600,
  fontSize: 25,
  opacity: 0.6,
});

export const cssButtonDatePicker = css({
  color: PRIMARY_COLOR,
  border: 0,
  backgroundColor: 'transparent',
  fontSize: 12,
});
