import Button from '@/components/Button';
import EmptyView from '@/components/Empty/view';
import Image from '@/components/Image';
import AppLayout from '@/components/Layout';
import SqModal from '@/components/SqModal';
import { DAYNAME } from '@/constants/days';
import { serializeFormData } from '@/helpers/form';
import { useMutation } from '@/hooks/useMutation';
import { PRIMARY_COLOR } from '@/utils/constant/style';
import { DatePicker } from '@material-ui/pickers';
import dayjs, { Dayjs } from 'dayjs';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { FiCalendar } from 'react-icons/fi';
import { FormGroup, Input } from 'reactstrap';
import GetQueueButton from './components/GetQueueButton';
import ambilAntrianMutation from './queries/ambilAntrian.gql';
import { cssButtonDatePicker, cssMerchantTitle } from './styles';

const data = {
  merchant: {
    __typename: 'Merchant',
    id: '8fb160c0-b191-45bc-b1ce-43b5a4c560c0',
    name: 'Klinik DR. Aminah',
    description: 'BCA',
    pictures: ['https://esensia.co.id/wp-content/uploads/2021/04/klinik-esensia-depan.jpgs'],
    openDay: ['sun'],
    closeHour: '23:00',
    openHour: '07:00',
    isOpen: true,
    merchantQueues: [
      {
        __typename: 'MerchantQueue',
        id: 'a18f3b61-836b-4ae2-afda-6cfb9870d705',
        quota: 100,
        status: 'Not Started',
        currentNumber: 0,
        lastNumber: 1,
        startDate: '2021-09-19T00:00:00.000Z',
        endDate: '2021-09-19T07:00:00.000Z',
      },
    ],
  },
};

const refetch = () => {};
const MerchantPage = () => {
  const { query } = useRouter();
  const datePicker: React.LegacyRef<HTMLInputElement> = useRef(null);

  const { id } = query;

  const [openedModal, setOpenedModal] = useState(false);
  const [selectedDate, setSelectedDate] = useState(dayjs());

  const openHour = useRef();

  // const { data, refetch, called } = useQuery(merchantQuery, {
  //   variables: {
  //     id,
  //     date: dayjs(
  //       `${selectedDate.format('YYYY-MM-DD')} ${openHour.current || dayjs().format('HH:mm')}:00`
  //     ).toISOString(),
  //   },
  // });

  const [doAmbil, { data: dataMutation }] = useMutation(ambilAntrianMutation, {
    onCompleted: () => {
      alert('Antrian anda telah terdaftar');
      setOpenedModal(false);
      refetch();
      window.location.replace('/antrian/cbfadb82-320f-4471-a6c4-fc28f9601c73');
    },
    onError: ({ message }) => {
      alert(message);
    },
  });
  useEffect(() => {
    if (openedModal && datePicker.current) {
      datePicker.current.value = dayjs().format('YYYY-MM-DD');
    }
  }, [openedModal]);

  const { merchant = {} as any } = data || {};
  const btnOpener = useRef(null);
  const { isOpen: remoteOpen, merchantQueues } = merchant;

  const isEmptyQueue = !merchantQueues?.length;
  useEffect(() => {
    if (!openHour.current) {
      openHour.current = merchant.openHour;
    }
  }, [merchant]);

  const queueData = merchantQueues?.[0] || {};
  const isOpen = true; //remoteOpen && dayjs().isBefore(dayjs(queueData.endDate));

  const CloseMerchant = <EmptyView height={200} title="Antrian sedang tutup, kembali lagi lain waktu" />;

  const OpenMerchant = isEmptyQueue ? (
    <EmptyView height={200} title="No Queue Data" />
  ) : (
    <>
      <p css={{ textAlign: 'center', fontSize: 14, color: '#9b9b9b', marginBottom: 0, marginTop: 30 }}>
        Antrian {selectedDate.format('DD MMMM YYYY')}
      </p>
      <h2 css={{ textAlign: 'center', marginTop: 0, fontSize: 80, fontWeight: 8, color: PRIMARY_COLOR }}>
        {queueData.currentNumber}
        <span css={{ fontSize: 40, color: '#ababab' }}> / {queueData.quota}</span>
      </h2>
      {queueData.lastNumber === 1 && <p css={{ textAlign: 'center', color: '#7b7b7b' }}>Kamu yang pertama !!</p>}
      {queueData.currentNumber === 0 && (
        <p css={{ textAlign: 'center', color: '#7b7b7b' }}>Sabar, antrian belum dimulai !!</p>
      )}
      <GetQueueButton onClick={() => setOpenedModal(true)} number={queueData.lastNumber} />
    </>
  );
  const handleSubmitDaftar = (e) => {
    e.preventDefault();
    const data = serializeFormData(e);
    console.log(data);

    if (!data.email && !data.hp) {
      alert('Anda harus mengisi setidaknya email atau HP');
      return;
    }

    alert('Berhasil mengambil antrian');
    window.location.replace('/antrian/09029def-9952-4531-98da-30ec19fd0edf');
  };
  const openDays = useMemo(() => {
    if (merchant && merchant.openDay) {
      if (merchant.openDay == 'weekdays') {
        return ['mon', 'tue', 'wed', 'thu', 'fri'];
      }
      return merchant.openDay;
    }
    return [];
  }, [merchant]);

  const getShouldDisbled = (date: Dayjs): boolean => {
    if (merchant.openDay?.includes('*')) {
      return false;
    }
    return !openDays.includes(date.format('ddd').toLowerCase());
  };
  return (
    <AppLayout actionBack title={merchant.name}>
      <Head>
        <title>{merchant.name}</title>
      </Head>
      <div css={{ position: 'relative', width: '100%', minHeight: 'calc(100vh - 56px)' }}>
        <Image
          width="100%"
          height={200}
          css={{ objectFit: 'cover' }}
          src="https://esensia.co.id/wp-content/uploads/2021/04/klinik-esensia-depan.jpg"
        />
        <div css={{ padding: '16px 20px' }}>
          <div css={{ display: 'flex', alignItems: 'center' }}>
            <h1 css={cssMerchantTitle}>{merchant.name}</h1>
            <button css={cssButtonDatePicker} onClick={() => btnOpener.current.click()}>
              <FiCalendar size={40} />
            </button>
          </div>
          <DatePicker
            disablePast
            shouldDisableDate={getShouldDisbled}
            innerRef={btnOpener}
            hidden
            value={selectedDate}
            onChange={(date) => {
              setSelectedDate(date);
            }}
          />
          <div css={{ opacity: 0.6, display: 'flex', alignItems: 'center' }}>
            <small css={{ marginLeft: 5 }}>
              Buka {DAYNAME[merchant.openDay]} pukul {merchant.openHour} - {merchant.closeHour}
            </small>
          </div>
        </div>
        {isOpen ? OpenMerchant : CloseMerchant}
      </div>
      <SqModal toggle={() => setOpenedModal((prev) => !prev)} isOpen={openedModal} title="Daftar Antrian">
        <form onSubmit={handleSubmitDaftar}>
          <FormGroup>
            <label>
              Email <small css={{ opacity: 0.4 }}>(Optional)</small>
            </label>
            <Input name="email" autoFocus placeholder="Masukan Email Kamu" />
          </FormGroup>
          <FormGroup>
            <label>
              Nomor HP <small css={{ opacity: 0.4 }}>(Optional)</small>
            </label>
            <Input name="hp" autoFocus placeholder="Masukan Nomor HP Kamu" />
          </FormGroup>
          <Button type="submit" css={{ marginTop: 50 }} block>
            DAFTAR
          </Button>
        </form>
      </SqModal>
    </AppLayout>
  );
};

export default MerchantPage;
