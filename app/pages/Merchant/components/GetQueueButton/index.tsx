import { PRIMARY_COLOR } from '@/utils/constant/style';
import React from 'react';
import { FC } from 'react';

interface GetQueueButtonProps {
  disabled?: boolean;
  number: number;
  onClick: () => void;
}

const GetQueueButton: FC<GetQueueButtonProps> = ({ disabled, number, onClick }) => {
  return (
    <button
      disabled={disabled}
      onClick={onClick}
      css={{
        display: 'block',
        position: 'fixed',
        bottom: 0,
        color: 'white',
        maxWidth: 500,
        width: '100%',
        backgroundColor: !disabled ? PRIMARY_COLOR : '#cbcbcb',
        cursor: 'pointer',
        border: 'none',
        padding: 16,
        fontWeight: 700,
      }}
    >
      Ambil Nomor {number}
    </button>
  );
};

export default GetQueueButton;
