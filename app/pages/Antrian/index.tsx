import AppLayout from '@/components/Layout';
import { NEXT_PUBLIC_BASE_URL } from '@/constants';
import { PRIMARY_COLOR } from '@/utils/constant/style';
import dayjs from 'dayjs';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from 'react';
import { QRCode } from 'react-qrcode-logo';

const data: any = {
  queueInfo: {
    __typename: 'UserQueueInfo',
    id: '09029def-9952-4531-98da-30ec19fd0edf',
    userPhone: '6285937812',
    userEmail: '',
    dateTimeEstimation: '2021-09-15T17:20:00.000Z',
    queueNumber: 2,
    merchantQueue: {
      __typename: 'MerchantQueue',
      quota: 100,
      currentNumber: 0,
      merchant: { __typename: 'Merchant', name: 'Barber Simpulasi Situbondo', address: 'Jalan keputih' },
    },
  },
};

const AntrianDetail = () => {
  const {
    query: { id },
  } = useRouter();

  const { queueNumber, merchantQueue, dateTimeEstimation } = data?.queueInfo || {};

  const { quota, currentNumber, merchant } = merchantQueue || {};
  const format = dayjs(dateTimeEstimation).isSame(dayjs(), 'date') ? 'HH:mm ' : 'DD MMMM YYYY HH:mm';

  return (
    <AppLayout title="Detail Antrian">
      <Head>
        <title>Detail Antrian</title>
      </Head>
      <div css={{ padding: 16, textAlign: 'center', marginTop: 30 }}>
        <h5 css={{ fontWeight: 100 }}>Halo, terima kasih telah menunggu</h5>
        <h6 css={{ fontWeight: 100, marginTop: 40 }}>Berikut nomor antrian kamu</h6>
        <h1 css={{ fontSize: queueNumber > 10000 ? '5rem' : '10rem', color: PRIMARY_COLOR, fontWeight: 700 }}>
          {queueNumber}
        </h1>
        <h6>
          Estimasi giliran kamu <span css={{ fontWeight: 700 }}>{dayjs(dateTimeEstimation).format(format)}</span>
        </h6>
        {currentNumber ? (
          <h6>
            Nomor Saat Ini {currentNumber} dari {quota}
          </h6>
        ) : (
          <h6>Antrian belum dimulai</h6>
        )}
        <div css={{ marginTop: 50 }}>
          <QRCode size={200} value={`${NEXT_PUBLIC_BASE_URL}/antrian/${id}`} />
        </div>
      </div>
    </AppLayout>
  );
};

export default AntrianDetail;
