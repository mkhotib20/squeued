import AppLayout from '@/components/Layout';
import useLocality from '@/helpers/geolocation/hooks/useLocality';
import useQuery from '@/hooks/useQuery';
import React from 'react';
import FeaturedCard from './components/FeaturedCard';
import Hero from './components/Hero';
import { HomePageDataProvider } from './context';
import merchantsQuery from './queries/merchants.gql';

const HomePage = () => {
  useLocality();

  const merchants = [
    {
      __typename: 'Merchant',
      id: '6516a783-2b39-4207-a00c-061b0cbe427b',
      name: 'Bank Mandiri Situbondo',
      image: 'https://cdn-asset.jawapos.com/wp-content/uploads/2020/12/BANK-MANDIRI3.jpg',
    },
    {
      __typename: 'Merchant',
      id: '2fb160c0-b191-45bc-b1ce-43b5a4c560c0',
      name: 'Barbershop Macho Situbondo',

      image: 'https://pbs.twimg.com/profile_images/580333070730993664/UINyIS7n_400x400.jpg',
    },
    {
      __typename: 'Merchant',
      id: 'ecefbdd9-ade6-41fb-8588-3328411dd84b',
      name: 'Klinik Dr. Aminah',
      image: 'https://esensia.co.id/wp-content/uploads/2021/04/klinik-esensia-depan.jpg',
    },
    {
      __typename: 'Merchant',
      id: '9fb160c0-b191-45bc-b1ce-43b5a4c560c0',
      name: 'Bank BRI Situbondo',

      image: 'https://disk.mediaindonesia.com/thumbs/600x400/news/2020/05/c6d52c16b1aa9e843a30f78abe9089a1.jpg',
    },
    {
      __typename: 'Merchant',
      id: '8fb160c0-b191-45bc-b1ce-43b5a4c560c0',
      name: 'Bank BCA Situbondo',

      image: 'https://biserje.com/wp-content/uploads/2019/04/gbr-01-ekonomiBisnis-bankBCA.jpg',
    },
  ];

  return (
    <AppLayout withBottomAction>
      <HomePageDataProvider merchants={merchants}>
        <Hero />
        {/* <CategorySelection /> */}
        <FeaturedCard />
      </HomePageDataProvider>
    </AppLayout>
  );
};

export default HomePage;
