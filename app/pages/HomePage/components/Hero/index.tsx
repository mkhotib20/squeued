import React from 'react';
import { cssHelloTitle, cssHelloWrapper, cssMauKemana } from './style';
import Address from '../Address';
import { useIntersect } from '@/hooks/useIntersect';

const Hero = () => {
  const ref = useIntersect(() => {});
  return (
    <div ref={ref} css={cssHelloWrapper}>
      <p css={cssHelloTitle}>Hi, Hanif</p>
      <h3 css={cssMauKemana}>
        Mau ngantri dimana
        <br /> hari ini ?
      </h3>
      <Address />
    </div>
  );
};

export default Hero;
