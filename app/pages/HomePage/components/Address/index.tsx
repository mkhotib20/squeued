import React from "react";
import { AiOutlineLoading3Quarters } from "react-icons/ai";
import { FiMapPin } from "react-icons/fi";
import { useHomeData } from "@/pages/HomePage/context";
import { cssAddress } from "./style";

const Searcher = () => {
  const { localityLoading, locality } = useHomeData();
  return (
    <button css={cssAddress}>
      {localityLoading ? (
        <AiOutlineLoading3Quarters className="spinner" color="#BABABA" size={20} />
      ) : (
        <FiMapPin color="#BABABA" size={20} />
      )}
      <span className="input">{localityLoading ? "Loading location . . " : locality}</span>
    </button>
  );
};

export default Searcher;
