import { css } from "@emotion/react";

export const cssAddress = css({
  display: "flex",
  alignItems: "center",
  height: 40,
  padding: "0 12px",
  color: "#BDBDBD",
  border: "1px solid #BDBDBD",
  borderRadius: 10,
  marginTop: 30,
  cursor: "pointer",
  "&>.input": {
    marginLeft: 14,
  },
  width: "100%",
  backgroundColor: "white",
});
