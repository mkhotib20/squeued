import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useState } from 'react';
import SingleItem from './SingleItem';
import { cssCategorySelection } from './style';

const categories = ['Gaya Hidup', 'Kesehatan', 'Layanan Public', 'Cafe dan Restoran'];

const CategorySelection = () => {
  const { query } = useRouter();
  const [sticky, setSticky] = useState(false);

  const handleStickyChanger = useCallback(() => {
    if (window.scrollY >= 210 && !sticky) {
      setSticky(true);
    } else {
      setSticky(false);
    }
  }, [sticky]);

  useEffect(() => {
    window.addEventListener('scroll', handleStickyChanger);
    return () => {
      window.removeEventListener('scroll', handleStickyChanger);
    };
  }, [handleStickyChanger]);

  return (
    <>
      {sticky && (
        <div css={cssCategorySelection(true)}>
          {categories.map((v, i) => (
            <SingleItem label={v} active={query.cat === v || (i === 0 && !query.cat)} key={i} />
          ))}
        </div>
      )}
      <div css={cssCategorySelection(false)}>
        {categories.map((v, i) => (
          <SingleItem label={v} active={query.cat === v || (i === 0 && !query.cat)} key={i} />
        ))}
      </div>
    </>
  );
};

export default CategorySelection;
