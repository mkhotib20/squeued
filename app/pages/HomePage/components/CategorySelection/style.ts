import { PRIMARY_COLOR } from "@/utils/constant/style";
import { css } from "@emotion/react";
import { keyframes } from "@emotion/react";

export const cssCategorySelection = (sticky: boolean) =>
  css({
    backgroundColor: "#fff",
    overflowX: "auto",
    overflowY: "hidden",
    padding: "10px 20px",
    whiteSpace: "nowrap",
    maxWidth: "90vw",
    "&::-webkit-scrollbar": {
      height: 1,
    },
    ...(sticky && {
      position: "fixed",
      left: 0,
      right: 0,
      top: 56,
      animation: kfAnimate,
      boxShadow: '0px 0px 4px #dbdbdb'
    }),
  });

const kfAnimate = keyframes({
  from: {
    transform: "translateY(-100px)",
  },
  to: {
    transform: "translateY(0)",
  },
});

export const cssSingleItemCategory = (active = false) =>
  css({
    display: "inline-block",
    borderRadius: 40,
    marginRight: 10,
    padding: "5px 15px",
    color: "#7F7F7F!important",
    transition: "backgroundColor .9s ease",
    ...(active && {
      backgroundColor: PRIMARY_COLOR,
      color: "white!important",
    }),
  });
