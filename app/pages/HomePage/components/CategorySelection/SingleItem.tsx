import React, { useEffect, useRef } from 'react';
import Link from 'next/link';
import { cssSingleItemCategory } from './style';

const SingleItem = ({ active = false, label = '' }) => {
  const scroller = useRef<HTMLAnchorElement>(null);
  useEffect(() => {
    if (active) {
      scroller.current?.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'center' });
    }
  }, [active]);
  return (
    <Link href={`?cat=${label}`}>
      <a ref={scroller} css={cssSingleItemCategory(active)}>
        {label}
      </a>
    </Link>
  );
};

export default SingleItem;
