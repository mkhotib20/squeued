import React from 'react';
import Skeleton from 'react-loading-skeleton';
import Link from 'next/link';
import { cssFeaturedCaption, cssFeaturedWraper } from './stye';

const FeaturedCardLoader = () => {
  return (
    <div css={cssFeaturedWraper}>
      <div css={cssFeaturedCaption}>
        <h1>
          <Skeleton width={113} count={2} height={29} />
        </h1>
        <Link href="/">
          <a className="link-to-queue">
            <Skeleton width={100} height={22} />
          </a>
        </Link>
      </div>
      <div className="featured-img">
        <Skeleton width={129.83} height={87} />
      </div>
    </div>
  );
};

export default FeaturedCardLoader;
