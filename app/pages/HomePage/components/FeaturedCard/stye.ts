import { PRIMARY_COLOR } from '@/utils/constant/style';
import { css } from '@emotion/react';

export const cssFeaturedWraper = css({
  padding: '0 50px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  marginTop: 30,
  marginBottom: 30,
  '&>.featured-img': {
    flex: 1,
    '&>img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    },
  },
});

export const cssFeaturedCaption = css({
  flex: 1,
  '&>h1': {
    fontSize: 26,
    fontWeight: 700,
    lineHeight: '30px',
    textAlign: 'left',
    margin: '0',
    marginRight: 20,
  },
  '&>.link-to-queue': {
    fontSize: 14,
    marginTop: 20,
    display: 'flex',
    alignItems: 'center',
    fontWeight: 700,
    color: PRIMARY_COLOR,
    '&>svg': {
      marginLeft: 10,
    },
  },
});
