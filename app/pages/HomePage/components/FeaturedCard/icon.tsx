import React from "react";

const ArrowRight = ({ color = "white" }) => {
  return (
    <svg width={14} height={14} viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1.1665 7H12.8332" stroke={color} strokeWidth="1.4375" strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M7 1.16602L12.8333 6.99935L7 12.8327"
        stroke={color}
        strokeWidth="1.4375"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ArrowRight;
