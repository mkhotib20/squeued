import React, { FC } from 'react';
import Image from '@/components/Image';
import { PRIMARY_COLOR } from '@/utils/constant/style';
import Link from 'next/link';
import { useHomeData } from '../../context';
import ArrowRight from './icon';
import FeaturedCardLoader from './loader';
import { cssFeaturedCaption, cssFeaturedWraper } from './stye';
import { canUseDOM } from '@/utils/constant';

const FeaturedCard: FC = () => {
  const { localityLoading, merchants = [] } = useHomeData();
  if (localityLoading && canUseDOM()) {
    return <FeaturedCardLoader />;
  }
  const renderSingleItem = ({ name, id,image }) => (
    <Link href={`/merchant/${id}`}>
      <a css={cssFeaturedWraper}>
        <div css={cssFeaturedCaption}>
          <h1>{name}</h1>
          <Link href="/">
            <a className="link-to-queue">
              <span>Get Queue</span> <ArrowRight color={PRIMARY_COLOR} />
            </a>
          </Link>
        </div>
        <div className="featured-img">
          <Image
            height={87}
            src={image}
            alt=""
          />
        </div>
      </a>
    </Link>
  );
  return <>{merchants.map((merchant) => renderSingleItem(merchant))}</>;
};

export default FeaturedCard;
