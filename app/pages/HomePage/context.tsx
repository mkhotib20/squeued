import useLocality from '@/helpers/geolocation/hooks/useLocality';
import React, { FC, useContext } from 'react';

interface Merchant {
  id: string;
  name: string;
  image: any
}
export interface HomePageContextType {
  locality?: string;
  localityLoading: boolean;
  merchants?: Merchant[];
}
export const homeDataContext = React.createContext<HomePageContextType>({
  localityLoading: true,
});
const { Provider } = homeDataContext;

export const useHomeData = () => useContext(homeDataContext);

export const HomePageDataProvider: FC<any> = ({ children, ...restData }) => {
  const { locality, loading: localityLoading } = useLocality();
  return <Provider value={{ localityLoading, locality, ...restData }}>{children}</Provider>;
};
