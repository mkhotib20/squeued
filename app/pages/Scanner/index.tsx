import Loading from '@/components/Loading';
import { withAuth } from '@/hoc/withAuth';
import dynamic from 'next/dynamic';

const ScannerPage = dynamic(() => import('./view'), { ssr: true, loading: Loading });

export default withAuth(ScannerPage);
