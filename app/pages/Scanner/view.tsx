import AppLayout from '@/components/Layout';
import dynamic from 'next/dynamic';
import React, { FC } from 'react';

const ScannerQr = dynamic<any>(() => import('react-qr-reader'), { ssr: false });

const ComponentScanner = () => {
  const handleScan = (data) => {
    if (data) {
      alert(data);
    }
  };
  return (
    <AppLayout title="Scan QR Code">
      <ScannerQr delay={300} onScan={handleScan} style={{ width: '100%', height: '100vh' }} />
    </AppLayout>
  );
};

export default ComponentScanner;
