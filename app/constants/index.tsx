export const { NEXT_PUBLIC_BACKEND_URL } = process.env;
export const { NEXT_PUBLIC_JWT_KEY } = process.env;
export const { NEXT_PUBLIC_BASE_URL } = process.env;
