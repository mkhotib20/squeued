export const DAYNAME = {
  mon: 'Senin',
  tue: 'Selasa',
  wed: 'Rabu',
  thu: 'Kamis',
  fri: 'Jumat',
  sat: 'Sabtu',
  sun: 'Minggu',
  weekdays: 'Senin - Jumat',
  '*': 'Setiap Hari',
};
