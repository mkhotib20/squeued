const withGraphQL = require("next-plugin-graphql")

// next.config.js
module.exports = withGraphQL({
	images: {
		domains: ["api.armos.net"],
	},
})
